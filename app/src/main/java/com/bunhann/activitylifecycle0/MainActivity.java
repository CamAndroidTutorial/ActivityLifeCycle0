package com.bunhann.activitylifecycle0;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    private static final String myTag = "LifeCycle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(myTag, "onCreate Invoked");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(myTag, "onStart Invoked");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(myTag, "onResume Invoked");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(myTag, "onPause Invoked");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(myTag, "onStop Invoked");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(myTag, "onRestart Invoked");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(myTag, "onDestroy Invoked");
    }
}
